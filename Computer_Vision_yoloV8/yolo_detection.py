from ultralytics import YOLO
import cv2 as cv
import os
import numpy as np
 
# Robot measurements in CM
c_zr = 22.5/100
c_xr = 12/100
f = 11

# Camera parameters
o_cx_pix = 334.55208066  # x-coordinate of the principal point in pixels
o_cy_pix = 259.00186612  # y-coordinate of the principal point in pixels

# Convert focal length from mm to pixels
f_x_pix =  695.10566639
f_y_pix =  659.75371531


def cartesian_to_polar(xr,yr):
    
    phi = np.arctan2(yr,xr)
    r = np.sqrt(xr**2 + yr**2)
    return phi,r
     
def camera_to_robot_system(xc,yc,zc,f,c_xr,c_zr):
    
    Ry = np.array([
                    [np.cos(f), 0, np.sin(f), -c_xr],
                    [0, 1, 0, 0],
                    [-np.sin(f), 0, np.cos(f), -c_zr],
                    [0, 0, 0, 1]
                   ]).reshape((4, 4))

    Rot = np.array([
                    [0,  0, 1, 0],
                    [-1, 0, 0, 0],
                    [0, -1, 0, 0],
                    [0,  0, 0, 1]
                   ]).reshape((4, 4))
 
    Tc = np.array([xc,yc,zc,1]).reshape((4, 1))
    
    Tr = np.dot(np.dot(Rot,Ry),Tc)
    
    return Tr[0],Tr[1],Tr[2],Tr[3]
     
def homogeneous_3d_coordinates(u_pix,v_pix,depth,o_cx_pix,o_cy_pix,f_x_pix,f_y_pix):
    
    Z = depth
    X =  -Z*(u_pix-o_cx_pix)/f_x_pix 
    Y =  -Z*(v_pix-o_cy_pix)/f_y_pix 
    W = 1

    return X/1000, Y/1000, Z/1000, W

def stereo_vision_depth(u1,u2, baseline, f_x_pix):
    disparity = np.abs(u1 - u2)
    depth = (baseline * f_x_pix) / disparity
    return depth
     
 
def capture_images():
    
    # Open the camera
    cap = cv.VideoCapture(0)  # 0 is the default camera, change it if you have multiple cameras

    num_images_to_capture = 1
    captured_images = []

    #print("Press 'C' to capture an image...")
    c=0
    while len(captured_images) < num_images_to_capture:
        # Capture frame-by-frame
        ret, frame = cap.read()

        # Display the frame
        #cv.imshow('Snapshot', frame)

        # Wait for the 'c' key to be pressed
        #key = cv.waitKey(1) & 0xFF
        #if key == ord('c') or key == ord('C'):
        captured_images.append(frame.copy())
        print(f"Image {len(captured_images)} captured!")

        # Break the loop if the 'q' key is pressed
        #elif key == ord('q') or key == ord('Q'):
            #break
        c=c+1

    # Release the camera and close the window
    cap.release()
    #cv.destroyAllWindows()

    return captured_images

     
def preprocess_image_for_yolo(image_path):
    
    # Read the input image
    img = cv.imread(image_path)

    # Apply Gaussian smoothing for noise reduction
    #img_smoothed = cv.GaussianBlur(img, (5, 5), 0)

    # Convert the image to grayscale
    #img_gray = cv.cvtColor(img_smoothed, cv.COLOR_BGR2GRAY)

    # Apply histogram equalization for contrast enhancement
    #img_equalized = cv.equalizeHist(img_gray)

    # Resize the image (adjust dimensions as needed)
    img_resized = cv.resize(img, (640, 640))  # YOLO standard input size

    # Apply morphological operations (optional)
    #kernel = np.ones((5, 5), np.uint8)
    #img_morph = cv.morphologyEx(img_resized, cv.MORPH_OPEN, kernel)

    # Save the preprocessed image
    #cv.imwrite('prepro_image.jpg', img)

    return img  # Return the preprocessed image for further YOLO processing
 
 
     
def detect_objects(model,captured_image):#, image_path, save_path):
    
    img =captured_image
    #img = preprocess_image_for_yolo(captured_image)#image_path)
    
    # Perform inference
    results = model.predict(img)

    # Access predictions
    predictions = results[0].boxes 
    
    conf_threshold = 0.3
    
    center_coords=[]
    horz_point_L=[]
    horz_point_R=[]
    
    # Draw bounding boxes on the image
    for pred in predictions:

        print(pred)
        
        class_id = int(pred.cls)
        label = model.names[class_id]
        confidence = pred.data[0][4]

        print(confidence)
        print(label)
        if (label.lower() == 'sports ball' 
            or label.lower() == 'vase'
            or label.lower() == 'orange') and confidence > conf_threshold:
            
            print("Object FOund !!!!!!!!!!")
            x1, y1, x2, y2 = map(int, pred.data[0][:4]) #* np.array([W, H, W, H])
            p0 = (x1,y1)
            p1 = (x2,y1)
            p2 = (x2,y2)
            p3 = (x1,y2)
 
            # Draw a dot at the center of the object
            # Calculate the center coordinates
            # Extract x and y coordinates
            x_coords = [point[0] for point in [p0, p1, p2, p3]]
            y_coords = [point[1] for point in [p0, p1, p2, p3]]

            # Calculate the midpoint coordinates
            midpoint_x = sum(x_coords) / len(x_coords)
            midpoint_y = sum(y_coords) / len(y_coords)

            center = (int(midpoint_x), int(midpoint_y))
            
            # Calculate midpoints on the horizontal lines
            middle_p1 = (int(x1),int(midpoint_y))
            middle_p2 = (int(x2),int(midpoint_y))
            
            horz_point_L.append(middle_p1)
            horz_point_R.append(middle_p2)
            # Draw bounding box
            #cv.rectangle(img, (p0[0],p0[1],p1[0],p1[1]), (0, 0, 255), 3)
            #cv.circle(img, center,1, (0, 255, 0), 10)
            #cv.circle(img, p0,1, (255, 0, 0), 10)
            #cv.circle(img, p1,1, (255, 0, 0), 10)
            #cv.circle(img, p2,1, (255, 0, 0), 10)
            #cv.circle(img, middle_p1,1, (0, 0, 255), 10)
            #cv.circle(img, middle_p2,1, (0, 0, 255), 10)
            #cv.circle(img, p3,1, (255, 0, 0), 10)


            # Display label and confidence
            #label_text = f"{label}: {confidence:.2f}"
            #cv.putText(img, label_text, (x1, y1 - 10), cv.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

    # Save the image with detected objects
    #cv.imwrite(save_path, img)
    return horz_point_L,horz_point_R

if __name__ == "__main__":
    
    # Load YOLOv8 model
    model = YOLO("yolov8x.pt")

    # Path to the image you want to test
    #image_path = "images/20.jpg"
    # Output path for the saved image with detections
    #output_path = "detected_image.jpg"
    
    # Capture images
    captured_image = capture_images()
    
    # Perform object detection and save the result
    horz_point_L ,horz_point_R= detect_objects(model,captured_image)#, image_path, output_path)

    #print(f"Detection results saved at: {output_path}")
    
    # Stereo Vision Principle for measuring depth
    ball_width = 42.67; #(mm to meters)
    
    coordinates = []
    
    for i in range(len(horz_point_L)):
        depth = stereo_vision_depth(horz_point_L[i][0], horz_point_R[i][0], ball_width, f_x_pix)
        xL,yL,zL,wL = homogeneous_3d_coordinates(horz_point_L[i][0], horz_point_L[i][1],depth,o_cx_pix,o_cy_pix,f_x_pix,f_y_pix)
        
        #coordinates.append([x,y,z,w])
        print("Camera Coordinates for Left Point",float(xL),float(yL),float(zL),float(wL))
        
        xR,yR,zR,wR = homogeneous_3d_coordinates(horz_point_R[i][0], horz_point_R[i][1],depth,o_cx_pix,o_cy_pix,f_x_pix,f_y_pix)
        #coordinates.append([x,y,z,w])
        print("Camera Coordinates for Right Point",float(xR),float(yR),float(zR),float(wR))
        
        x_center = (xL+xR)/2
        
        xr,yr,zr,wr = camera_to_robot_system(x_center,yR,zR,f,c_xr,c_zr)
        #coordinates.append([xr,yr,zr,wr])
        print("MOTOR relative Coordinates for Right Point",float(xr),float(xr),float(xr),float(xr))

        coordinates.append([xr,yr,zr,wr])
        
    min=999
    index_min = 0
    for j in range(len(coordinates)):
        
        if min<coordinates[j][2]:
            min = coordinates[j][2]
            index_min = j
    
    try:
        this = coordinates[index_min]
        phi,r = cartesian_to_polar(this[0],this[1])
        print("Polar Motor Coordinates to Object",phi,r)
    except:
        phi=-1
        r=-1
        print(phi,r) 
        
    
        
 
        
 
