/*  
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "sdist.h"

#include "bplan20.h"
#include "simu.h"
#include "bplanchallenge5ToFinish.h"

// create class object
BPlanchallenge5ToFinish planchallenge5ToFinish;


void BPlanchallenge5ToFinish::setup()
{ // ensure there is default values in ini-file
  if (not ini["planchallenge5ToFinish"].has("log"))
  { // no data yet, so generate some default values
    ini["planchallenge5ToFinish"]["log"] = "true";
    ini["planchallenge5ToFinish"]["run"] = "false";
    ini["planchallenge5ToFinish"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["planchallenge5ToFinish"]["print"] == "true";
  //
  if (ini["planchallenge5ToFinish"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_planchallenge5ToFinish.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plan20 logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission status (mostly for debug)\n");
  }
  setupDone = true;
}

BPlanchallenge5ToFinish::~BPlanchallenge5ToFinish()
{
  terminate();
}


void BPlanchallenge5ToFinish::run()
{


  if (not setupDone)
    setup();
  if (ini["Planchallenge5ToFinish"]["run"] == "false")
    return;
  //
  UTime t("now");
  bool finished = false;
  bool lost = false;
  double length = 1;
  state = 6;
  oldstate = state;
  int distt =0 ;
  toLog("Planchallenge5ToFinish started");
  //
   

   int state = 1;
  while (not finished and not lost and not service.stop)
  {


  cout << "hell yeah" << endl;

   
    
usleep(100);
//////END 


//cout << "plan started" << endl;




/*old code
//program drives from challenge 5 to the goal

  switch (state) {
    case 1:
      cout << "case 1" << endl;
      pose.resetPose();
      mixer.setDesiredHeading(3.14);
      sleep(2);
      mixer.setEdgeMode(false, 0);
      state = 2;
    break;

    case 2:
    cout << "case 2" << endl;
    //mixer.setVelocity(-0.2);
    //if(imu.gyro>40){sleep(3); mixer.setVelocity(0.0);}
      mixer.setVelocity(0.3);
      if(dist.dist[0] < 0.00001){
         cout << "distance "<< pose.dist << endl;

        state = 3;
      }
    break;
  case 3:
    mixer.setVelocity(0.0);
    cout << "finish!"<< endl;
    finished = true;
  break;
}
  usleep(500);


//end of program



  }*/



  if (lost)
  { // there may be better options, but for now - stop
    toLog("Planchallenge5ToFinish got lost");
    mixer.setVelocity(0);
    mixer.setTurnrate(0);
  }
  else
    toLog("Planchallenge5ToFinish finished");

    
}
}


void BPlanchallenge5ToFinish::terminate()
{ //
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void BPlanchallenge5ToFinish::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}

