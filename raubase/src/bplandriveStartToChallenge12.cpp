/*  
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "sdist.h"

#include "bplandriveStartToChallenge12.h"

#include "simu.h"
// create class object
bplandriveStartToChallenge12 plandriveStartToChallenge12;


//int a = imu.gyro[0];

void bplandriveStartToChallenge12::setup()
{ // ensure there is default values in ini-file
  if (not ini["plandriveStartToChallenge12"].has("log"))
  { // no data yet, so generate some default values
    ini["plandriveStartToChallenge12"]["log"] = "true";
    ini["plandriveStartToChallenge12"]["run"] = "false";
    ini["plandriveStartToChallenge12"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["plandriveStartToChallenge12"]["print"] == "true";
  //
  if (ini["plandriveStartToChallenge12"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_plandriveStartToChallenge12.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plandriveStartToChallenge12 logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission status (mostly for debug)\n");
  }
  setupDone = true;
  
}

bplandriveStartToChallenge12::~bplandriveStartToChallenge12()
{
  terminate();
}


void bplandriveStartToChallenge12::run()
{
 if (not setupDone){
    setup();}
  if (ini["plandriveStartToChallenge12"]["run"] == "false"){
    return;}
  //
  UTime t("now");
bool finished = false;
  bool lost = false;
  double length = 1;
  int angle = 0;
  state = 10;
  oldstate = state;
  toLog("driveStartToChallenge12 started");
  //
 
 cout << "diving up the ramp" << endl;


//////////////////////////////////////////////////////////////////////////////////////


   while (not finished and not lost and not service.stop)
  {

  cout << "hell yeah" << endl;

   
    
usleep(100);
//////END 
   






    if (state != oldstate)
    {
      oldstate = state;
      toLog("state start");
      // reset time in new state
      t.now();
    }
    // wait a bit to offload CPU
    usleep(2000);
  }




  if (lost)
  { // there may be better options, but for now - stop
    toLog("plandriveStartToChallenge12 got lost");
    mixer.setVelocity(0);
    mixer.setTurnrate(0);
  }
  else
    toLog("plandriveStartToChallenge12 finished");
}


void bplandriveStartToChallenge12::terminate()
{ //
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void bplandriveStartToChallenge12::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}

