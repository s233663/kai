/*  
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "sdist.h"

#include "bplan10_axe.h"

// create class object
BPlan10_axe plan10_axe;


void BPlan10_axe::setup()
{ // ensure there is default values in ini-file
  if (not ini["plan10_axe"].has("log"))
  { // no data yet, so generate some default values
    ini["plan10_axe"]["log"] = "true";
    ini["plan10_axe"]["run"] = "false";
    ini["plan10_axe"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["plan10_axe"]["print"] == "true";
  //
  if (ini["plan10_axe"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_plan10_axe.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plan10_axe logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission status (mostly for debug)\n");
  }
  setupDone = true;
}

BPlan10_axe::~BPlan10_axe()
{
  terminate();
}


void BPlan10_axe::run()
{
  if (not setupDone)
    setup();
  if (ini["plan10_axe"]["run"] == "false")
    return;
  //
  UTime t("now");
  bool finished = false;
  bool lost = false;
  double length = 1;
  state = 1;
  oldstate = state;
  int distt =0 ;
  //cedge.changePID(23,0.5,0.02,0.0);
  toLog("Plan10_axe started");
  //
  pose.resetPose();
	        
  while (not finished and not lost and not service.stop)
  {
    switch (state)
      {

        case 1:
        {
          mixer.setVelocity(0.1);
          mixer.setEdgeMode(true, 0);
          usleep(10000);

          if(pose.dist>1){
            mixer.setVelocity(0.0);
            state =2;
          }
          
          /*
          if (medge.width > 0.05){
            usleep(3700000);
            mixer.setVelocity(0.0);
            state = 2;
          }
          */

          break;
        }

        case 2:
        {
          if(dist.dist[0]<0.2){
            //first pass
            usleep(1000000);
            state =3;
          }
          break;
        }

        case 3:
        {
          //second pass to be sure it is a whole pass
          if(dist.dist[0]<0.2){
            usleep(100000);
            mixer.setVelocity(0.4);
            usleep(5000000);
            state =3;
          }
          break;
        }

        case 4:
        {
          mixer.setVelocity(0.0);
          finished = true;
          break;
        }

      }

    if (state != oldstate)
    {
      oldstate = state;
      toLog("state start");
      // reset time in new state
      t.now();
    }
    // wait a bit to offload CPU
    usleep(2000);
  
    if (lost)
    { // there may be better options, but for now - stop
      toLog("Plan10_axe got lost");
      mixer.setVelocity(0);
      mixer.setTurnrate(0);
    }
    else
      toLog("Plan10_axe finished");
  }
}


void BPlan10_axe::terminate()
{ //
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void BPlan10_axe::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}

