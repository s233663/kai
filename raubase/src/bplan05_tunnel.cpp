/*  
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "sdist.h"

#include "bplan05_tunnel.h"

// create class object
BPlan05_tunnel plan05_tunnel;


void BPlan05_tunnel::setup()
{ // ensure there is default values in ini-file
  if (not ini["plan05_tunnel"].has("log"))
  { // no data yet, so generate some default values
    ini["plan05_tunnel"]["log"] = "true";
    ini["plan05_tunnel"]["run"] = "false";
    ini["plan05_tunnel"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["plan05_tunnel"]["print"] == "true";
  //
  if (ini["plan05_tunnel"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_plan05_tunnel.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plan05_tunnel logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission status (mostly for debug)\n");
  }
  setupDone = true;
  
}

BPlan05_tunnel::~BPlan05_tunnel()
{
  terminate();
}


void BPlan05_tunnel::run()
{
  if (not setupDone)
    setup();
  if (ini["plan05_tunnel"]["run"] == "false")
    return;
  //
  UTime t("now");
  bool finished = false;
  bool lost = false;
  double length = 1;
  int angle = 0;
  state = 1;
  oldstate = state;
  toLog("Plan05_tunnel started");
  //
   while (not finished and not lost and not service.stop)
  {
    switch (state)
      {
      case 1:
	medge.recalWood();
	mixer.setEdgeMode(true, 0);
	mixer.setVelocity(0.1);
	while (dist.dist[0] > 0.1) {
	  usleep(10000);
	}
	pose.resetPose();
	state = 10;
	break;
      case 10:
	mixer.setVelocity(0);
	mixer.setDesiredHeading(M_PI * 0.5);
	sleep(2);
	pose.resetPose();
	mixer.setVelocity(0.2);
	state = 11;
	break;
	
      case 11:
	if(pose.dist >= 0.65) {
	  mixer.setVelocity(0);
	  mixer.setDesiredHeading(-M_PI/2);
	  sleep(2);
	  pose.resetPose();
	  state = 110;
	} else {
	  usleep(10000);
	}
	break;

      case 110:
	mixer.setVelocity(0.22);
	sleep(2);
	mixer.setVelocity(0);
	mixer.setDesiredHeading(M_PI * 0.5);
	sleep(2);
	pose.resetPose();
	state = 12;
	break;
	
      case 12:
	mixer.setVelocity(-0.1);
	sleep(6);
	mixer.setVelocity(0.1);
	sleep(1);
	mixer.setDesiredHeading(-M_PI/2);
	sleep(2);
	mixer.setVelocity(-0.2);
	state = 13;
	break;
	
      case 13:
	if(dist.dist[1] > 0.3) {
	  usleep(2000);
	  if(dist.dist[1] > 0.3) {
	    mixer.setVelocity(0);
	    state = 14;
	  }
	} else {
	  usleep(50000);
	}
	break;
	
      case 14:
	usleep(10000);
	mixer.setVelocity(0);
	mixer.setDesiredHeading(0);
	sleep(2);
	pose.resetPose();
	mixer.setVelocity(-0.5);
	sleep(1);
	mixer.setVelocity(0);
	mixer.setDesiredHeading(-M_PI/2);
	sleep(2);
	mixer.setEdgeMode(true, 0);
	mixer.setVelocity(0.5);
	finished = true;
	break;
      }
    // switch (state)
    // { // make a shift in heading-mission
    //   case 10:    //     pose.resetPose();
    //     toLog("forward at 0.3m/s");
    //     mixer.setVelocity(0.3);
    //     state = 11;
    //     break;
    //   case 11: // wait for distance
    //     if (pose.dist >= 1.0)
    //     { // done, and then
    //       finished = true;
    //     }
    //     else if (t.getTimePassed() > 10)
    //       lost = true;
    //     break;
    //   default:
    //     toLog("Unknown state");
    //     lost = true;
    //     break;
    // }
    if (state != oldstate)
    {
      oldstate = state;
      toLog("state start");
      // reset time in new state
      t.now();
    }
    // wait a bit to offload CPU
    usleep(2000);
  }
  if (lost)
  { // there may be better options, but for now - stop
    toLog("Plan05_tunnel got lost");
    mixer.setVelocity(0);
    mixer.setTurnrate(0);
  }
  else
    toLog("Plan05_tunnel finished");
}


void BPlan05_tunnel::terminate()
{ //
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void BPlan05_tunnel::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}

