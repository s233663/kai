/*  
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "simu.h"


#include "sdist.h"
#include "bplan100.h"


// create class object
BPlan100 plan100;


void BPlan100::setup()
{ // ensure there is default values in ini-file
  if (not ini["plan100"].has("log"))
  { // no data yet, so generate some default values
    ini["plan100"]["log"] = "true";
    ini["plan100"]["run"] = "false";
    ini["plan100"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["plan100"]["print"] == "true";
  //
  if (ini["plan100"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_plan100.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plan100 logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission state (mostly for debug)\n");
  }
  setupDone = true;
}

BPlan100::~BPlan100()
{
  terminate();
}


void BPlan100::run()
{
  if (not setupDone)
    setup();
  if (ini["plan100"]["run"] == "false")
    return;
  //
  UTime t("now");
  float curr_h = 0;
  bool finished = false;
  bool lost = false;
  state = 106;
  oldstate = state;
  //
  toLog("Plan100 started");
  //
  
  while (not finished and not lost and not service.stop)
  {

//starting position is after the racetrack before the ramp, faceing the ramp

  switch (state)
  {
    case 0:{
      cout<<"turn"<<endl;
      pose.resetPose();
      mixer.setDesiredHeading(M_PI/5);
      usleep(1500000);
      mixer.setEdgeMode(true, 0);
      mixer.setVelocity(0.2);
      sleep(1);
      state =1;
      break;
    }
    case 1: 
      cout<<"start driving up the ramp"<<endl;
      pose.resetPose();
      mixer.setEdgeMode(true, 0);
      mixer.setVelocity(0.2);
      
      if (dist.dist[1]<0.2){       // detect first intersection
          cout << "detected first gate" << endl;
          state = 100;
          usleep(1000000);
      //   cout << "detect first intersection" << endl;
      //   mixer.setVelocity(0.2);
      //   usleep(900000);
       // pose.resetPose();
      }
      break;
      
    
    case 100:
      if (dist.dist[1]<0.2){
        state  = 101;
        cout<< "detect second gate"<< endl;
        //pose.resetPose();
        //mixer.setEdgeMode(true,0);
        usleep(14000000);
      }
    break;
    case 101:{
      //if (pose.dist>5){
        state  = 102;
        cout<< "detect third gate"<< endl;
        usleep(3000000);
        
        mixer.setVelocity(0);
        
        usleep(10000);
        pose.resetPose();

        state = 102;

      //}
      break;
    }

    case 102:
    {
      mixer.setDesiredHeading(M_PI);
      usleep(1000000);
      cout<<"servo down"<<endl;
      servo.setServo(4,1,-100,0);
      usleep(1000000);
      mixer.setEdgeMode(false,0);
      usleep(1000000);
      mixer.setVelocity(0.2);
      sleep(2);

      state =103;

      break;
    }
    
    case 103:{
      if(medge.width>0.05)
      {
        pose.resetPose();
	usleep(500000);
        mixer.setVelocity(0.0);
        mixer.setDesiredHeading(M_PI/2);
	sleep(2);
	mixer.setVelocity(0.1);
	sleep(3);
        usleep(1000000);
        state =104;
      } else {
	mixer.setVelocity(0.2);
	mixer.setEdgeMode(true, 0);
	usleep(10000);
      }
      break;
    }
    case 104:
      if (medge.edgeValid) {
	servo.setServo(4,1,0,0);
	mixer.setEdgeMode(false,0);
	mixer.setVelocity(0.1);
	usleep(10000);
      } else {
	cout << "Line not valid" << endl;
	sleep(1);
	state = 105;
      }
      break;

    case 105: {
      if (medge.edgeValid) {
	sleep(2);
	state =106;
      } else {
	usleep(10000);
	mixer.setVelocity(0.1);
	mixer.setEdgeMode(false, 0);
      }
      break;
    }

    case 106:{
      cout<<"found line"<<endl;
      mixer.setEdgeMode(true,0);
      mixer.setVelocity(0.3);
      medge.recalWood();
      servo.setServo(4, 1, -500, 0);
      state = 1063;
      break;
    }
  case 1063:
    if (medge.width > 0.06) {
      cout << "Line is ending" << endl;
      pose.resetPose();
      mixer.setDesiredHeading(0);
      mixer.setVelocity(0.1);
      sleep(2);
      state = 1066;
    } else {
      usleep(10000);
    }
    break;
  case 1066:
    if (medge.width > 0.03) {
      servo.setServo(4, 1, -200, 0);
      sleep(1);
      mixer.setVelocity(0);
      mixer.setDesiredHeading(-M_PI/2);
      sleep(2);
      state = 107;
    } else {
      usleep(10000);
    }
    break;

  case 107:
    mixer.setEdgeMode(false, 0);
    if (dist.dist[1] > 0.35) {
      mixer.setVelocity(0.2);
      usleep(5000);
    } else {
      state = 108;
    }
    break;
  case 108:
    if (medge.width > 0.05) {
      cout << "Splittt" << endl;
      sleep(1);
      pose.resetPose();
      mixer.setVelocity(0);
      mixer.setDesiredHeading(-M_PI);
      sleep(2);
      mixer.setVelocity(-0.1);
      sleep(1);
      mixer.setVelocity(0);
      pose.resetPose();
      state = 109;
    } else {
      usleep(10000);
      mixer.setVelocity(0.3);
    }
    break;
  case 109:
    if (pose.dist > 0.8) {
      mixer.setVelocity(0);
      servo.setServo(4, 1, 200, 0);
      sleep(2);
      state = 110;
    } else {
      usleep(10000);
      mixer.setVelocity(0.1);
      mixer.setEdgeMode(true, 0);
    }
    break;
  case 110:
    mixer.setVelocity(0.1);
    mixer.setEdgeMode(true, 0);
    sleep(16);
    state = 111;
    break;
  case 111:
    if (medge.width > 0.05) {
      pose.resetPose();
      mixer.setVelocity(0.2);
      mixer.setDesiredHeading(-M_PI/4);
      sleep(3);
      mixer.setEdgeMode(true, 0);
    } else {
      usleep(10000);
    }
    break;
      
    
  }

/*UTime t("now");
  bool finished = false;
  bool lost = false;
  int state = 0;
  int memory=0;
  int counter = 0;
  int flag = 0;
  int oldstate = state;
  int id = 0;
  int found = 0;
  float r=0.0;
  float phi=0.0;
  char REQUEST = "\n";*/
    /*
    case 105:{
      if(pose.h-curr_h > M_PI/3){
        cout<<"servo out"<<endl;
        servo.setServo(4,1,-500,0);
        mixer.setVelocity(0);
        usleep(100000);
        finished =true;
      }
    }
    */

//     case 2:
//      //cout << "detect first intersection " << endl;
//       if (medge.width> 0.05){       // detect second intersection
//         mixer.setVelocity(0);
//         pose.resetPose();
//         cout << "detect intersection to the seesaw" << endl;
//         servo.setServo(4,1,0,0);
//         sleep(1);
//         // mixer.setVelocity(-0.05);                            //  drive a litte backwarts to not miss the intersection
//          cout << "Pose " << pose.turned /3.14 << endl;
//          sleep(1);
//         //  mixer.setVelocity(-0.05);
//         //  usleep(1000000);
//          mixer.setVelocity(0);
//          usleep(1000000);
//         mixer.setDesiredHeading((3.14 * 0.5));             // turn towarts the seesaw
//         sleep(2);
//         state = 3;
//         //sleep(5);

        
//       }
//     break;
  
//     case 3:
//  mixer.setVelocity(0.08);
//  mixer.setEdgeMode(true, 0);                        // true when we want to go to the left after the seesaw
//      cout << "Pose 3 " << pose.turned /3.14 << endl;
//      sleep(2);
//      servo.setServo(4,1,-500,0);
//      sleep(2);
//      state = 4;
//     break;




//     case 4:         //now on seesaw
//        cout << "case 4" << endl;
//       if(medge.width> 0.05){  //detect new line after the seesaw
   
      
//         sleep(4);
//         mixer.setVelocity(0.2);
//          cout << "after seesaw" << endl;
//         finished = true;


//         sleep(100);
//       }
//     break;

  
//   }


// usleep(200);

//     ///////////////////////////////////end of my code

//     ////////////////////////////////////origional code, ignore

// //start code down the stairs on the ramp . WE might skip this challenge, because the extra weights makte it very difficult probably
// switch (state)
// {
//   case 0:
//       mixer.setEdgeMode(true, 0);
//       mixer.setVelocity(0.2);
//       usleep(100000);
//       cout << "dist " << pose.dist << " Splits " << medge.split << " heading " << pose.h << endl;
//       if (pose.h < -M_PI * 0.8) {
// 	pose.resetPose();
// 	mixer.setVelocity(0);
// 	mixer.setDesiredHeading(-M_PI);
// 	sleep(2);
// 	mixer.setVelocity(0.2);
// 	state = 1;
//       }
//     break;
//   case 1:
//      if(medge.width> 0.05){   //detect the intersection before the stairs
//        state = 2;
//      } else {
//        usleep(10000);
//      }
//   break;

//   case 2:
//     mixer.setVelocity(0);
//     pose.resetPose();
//     cout << "stop " << endl;
//     sleep(0.5);
//     mixer.setDesiredHeading((3.1415));
//     cout << "turn" << endl;
//     sleep(3);
//     servo.setServo(4,1,0,0);
//     sleep(3);
//     cout<<"down servo"<< endl;
//     pose.resetPose();
//     mixer.setDesiredHeading((3.1415 * 0.2));
//     sleep(1);
//     mixer.setVelocity(-0.6);
//     cout << "back" << endl;
//     sleep(0.5);
//     // mixer.setVelocity(0.1);
//     cout << "aling" << endl;
//     //sleep(0.5);
//     state = 3;
//   break;

//   case 3:
//     mixer.setVelocity(-0.4);
//     sleep(1);
//     //cout << "gyro "<< imu.gyro[1]  << endl;
//     //cout << "gyro "<< imu.gyro[1]  << endl;
//     if(imu.gyro[1]  > 150){
//       cout << "1st drop" << endl;
//       cout << "gyro "<< imu.gyro[1]  << endl;
//       usleep(350000);
//       mixer.setVelocity(0.0);
//       usleep(5000000);
//       mixer.setVelocity(0.05);
//       usleep(1000000);
//       state = 4;
//     }
//   break;

//   case 4:
//     mixer.setVelocity(-0.4);
//     sleep(1);
//     //cout << "gyro "<< imu.gyro[1]  << endl;
//     //cout << "gyro "<< imu.gyro[1]  << endl;
//     if(imu.gyro[1]  > 150){
//       cout << "2nd drop" << endl;
//       cout << "gyro "<< imu.gyro[1]  << endl;
//       usleep(350000);
//       mixer.setVelocity(0.0);
//       usleep(5000000);
//       mixer.setVelocity(0.05);
//       usleep(1000000);
//       state = 5;
//     }
//   break;

//   case 5:
//     mixer.setVelocity(-0.6);
//     sleep(1);
//     //cout << "gyro "<< imu.gyro[1]  << endl;
//     //cout << "gyro "<< imu.gyro[1]  << endl;
//     if(imu.gyro[1]  > 150){
//       cout << "3rd drop" << endl;
//       cout << "gyro "<< imu.gyro[1]  << endl;
//       usleep(350000);
//       mixer.setVelocity(0.0);
//       usleep(5000000);
//       mixer.setVelocity(0.05);
//       usleep(1000000);
//       state = 6;
//     }
//   break;

//   case 6:
//     mixer.setVelocity(-0.6);
//     sleep(1);
//     //cout << "gyro "<< imu.gyro[1]  << endl;
//     //cout << "gyro "<< imu.gyro[1]  << endl;
//     if(imu.gyro[1]  > 150){
//       cout << "4th drop" << endl;
//       cout << "gyro "<< imu.gyro[1]  << endl;
//       usleep(350000);
//       mixer.setVelocity(0.0);
//       usleep(5000000);
//       mixer.setVelocity(0.05);
//       usleep(1000000);
//       state = 7;
//     }
//   break;

//   case 7:
//     mixer.setVelocity(-0.6);
//     sleep(1);
//     //cout << "gyro "<< imu.gyro[1]  << endl;
//     //cout << "gyro "<< imu.gyro[1]  << endl;
//     if(imu.gyro[1]  > 150){
//       cout << "5th drop" << endl;
//       cout << "gyro "<< imu.gyro[1]  << endl;
//       usleep(350000);
//       mixer.setVelocity(0.0);
//       usleep(1000000);
//       mixer.setVelocity(0.2);
//       usleep(1000000);
//       state = 8;
//       mixer.setVelocity(-0.2);
//       sleep(1);
//       mixer.setVelocity(0);
//       sleep(1);
//       pose.resetPose();
//       mixer.setDesiredHeading(-3.6);
//       sleep(2);
//       servo.setServo(4,1,-500,0);
//       sleep(3);
//       mixer.setVelocity(0.2);
//       mixer.setEdgeMode(false, 0);
//       cout << "look for line" << endl;
//     }
//   break;

//   case 8:
//     if (medge.width > 0.05){
//       cout << "intersection found" << endl;
//       mixer.setVelocity(0);
//       mixer.setDesiredHeading(1*3.1415);
//       sleep(5);
//       cout << "heading found" << endl;
//       mixer.setEdgeMode(true, 0);
//       mixer.setVelocity(0.1);
//       state = 9;
//     }
//   break;

//   case 9:
//     cout << "dis  " << dist.dist[0] << endl;
//     mixer.setVelocity(0.2);
//     if(dist.dist[0] < 0.10){
//       sleep(3);
//       cout << "end" << endl;
//       state = 10;
//    }
//   break;

//   case 10:
//     mixer.setVelocity(0);
//     finished = true;
//     sleep(100);
//   break;


usleep(200);
    if (state != oldstate)
    {
      oldstate = state;
      toLog("state start");
      // reset time in new state
      t.now();
    }
    // wait a bit to offload CPU
    usleep(2000);
    }

      
  if (lost)
  { // there may be better options, but for now - stop
    toLog("Plan100 got lost");
    mixer.setVelocity(0);
    mixer.setTurnrate(0);
  }
  else
    toLog("Plan100 finished");

}



void BPlan100::terminate()
{ // just close logfile
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void BPlan100::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}


