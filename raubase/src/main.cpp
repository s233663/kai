/*
 #***************************************************************************
 #*   Copyright (C) 2023 by DTU
 #*   jcan@dtu.dk
 #*
 #* The MIT License (MIT)  https://mit-license.org/
 #*
 #* Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 #* and associated documentation files (the “Software”), to deal in the Software without restriction,
 #* including without limitation the rights to use, copy, modify, merge, publish, distribute,
 #* sublicense, and/or sell copies of the Software, and to permit persons to whom the Software
 #* is furnished to do so, subject to the following conditions:
 #*
 #* The above copyright notice and this permission notice shall be included in all copies
 #* or substantial portions of the Software.
 #*
 #* THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 #* INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 #* PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 #* FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 #* ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 #* THE SOFTWARE. */

// System libraries
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <string>
//
// include local files for data values and functions
#include "uservice.h"
#include "cmixer.h"
#include "sgpiod.h"
#include "bplan20.h"
#include "bplan21.h"
#include "bplan40.h"
#include "bplan100.h"
#include "bplan101.h"
#include "bplan05_tunnel.h"
#include "bplan_three_gate.h"
#include "bplandriveStartToChallenge12.h"
#include "bplandrive12to10.h"
#include "bplanchallenge5ToFinish.h"
#include "bplan_racetrack.h"
#include "bplan10_axe.h"
#include "medge.h"
#include "cservo.h"
#include "bplan_golf.h"


// DELETE THIS
#include "sdist.h"


int main (int argc, char **argv)
{ // prepare all modules and start data flow
  // but also handle command-line options
  service.setup(argc, argv);
  //
  if (not service.theEnd)
  { // all set to go
    // turn on LED on port 16 
    gpio.setPin(16, 1);

    // run the planned missions

    /*
    while(true){
      cout<<dist.dist[0]<<endl;
      //mixer.setVelocity(0.1);
      //mixer.setEdgeMode(true,0.01);
      usleep(10000);
    }*/
    ///////////////////START OF TESTING///////////////
    //First starting from 3 gates
    plan100.run();
    plan_three_gate.run();
    //plan40 is for axe gate second to execute
    // plan40.run();

    // plan05_tunnel.run();

    //racetrack is 4th +close gates
    plan9.run();

    //go up the ramp
    plan100.run();

    sleep(2);



    ///////////////////END OF TESTING///////////////

/*
while(true){
cout<<medge.width<<endl;
//mixer.setVelocity(0.1);
//mixer.setEdgeMode(true,0.01);
usleep(10000);
}
*/
//usleep(10000000);
//plan 3 gate is the initial plan


//turn 180 degrees
/*
mixer.setTurnrate(1.99);
mixer.setVelocity(0);
sleep(2);
mixer.setTurnrate(0);
// 2-2 ->210
pose.resetPose();
sleep(2);
*/
//mixer.setDesiredHeading(M_PI);
//sleep(3);
//golf.run();


//servo.setServo(4,1,-500,0.1);
//500 down
//-500 up
//sleep(7);

/*

mixer.setVelocity(0.8);
 mixer.setEdgeMode(false,0);
usleep(2000000);

while(true){
  mixer.setVelocity(1.5);
  mixer.setEdgeMode(false,0);
  //mixer.setTurnrate(0);
  sleep(1);
  cout<<dist.dist[1]<<endl;
}
*/

//mixer.setDesiredHeading(M_PI/2);
//usleep(5000000);

    // turn off led 16
    gpio.setPin(16, 0);
  }
  // close all logfiles etc.
  service.terminate();
  return service.theEnd;
}

