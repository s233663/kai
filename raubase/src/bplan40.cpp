/*  
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "sdist.h"

#include "bplan40.h"

// create class object
BPlan40 plan40;


void BPlan40::setup()
{ // ensure there is default values in ini-file
  if (not ini["plan40"].has("log"))
  { // no data yet, so generate some default values
    ini["plan40"]["log"] = "true";
    ini["plan40"]["run"] = "false";
    ini["plan40"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["plan40"]["print"] == "true";
  //
  if (ini["plan40"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_plan40.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plan40 logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission status (mostly for debug)\n");
  }
  setupDone = true;
}

BPlan40::~BPlan40()
{
  terminate();
}

//use for axe gate

void BPlan40::run()
{
  if (not setupDone)
    setup();
  if (ini["plan40"]["run"] == "false")
    return;
  //
  UTime t("now");
  bool finished = false;
  bool lost = false;
  double length = 1;
  state = 101;
  oldstate = state;
  int distt =0 ;
  //cedge.changePID(23,0.5,0.02,0.0);
  toLog("Plan40 started");
  //
  pose.resetPose();
	        
  while (not finished and not lost and not service.stop)
  {
    switch (state)
      {
      case 101:
	mixer.setVelocity(0.3);
	mixer.setEdgeMode(true, 0);
	if (medge.split > 0) {
	  mixer.setEdgeMode(true, 0);
	  sleep(3);
	  mixer.setVelocity(0.5);
	  state = 111;
	  medge.split = 0;
	} else {
	  usleep(10000);
	}
	break;
      case 111:
	if (medge.split == 0) {
	  sleep(3);
	  pose.resetPose();
	  mixer.setDesiredHeading(0);
	  sleep(1);
	  mixer.setEdgeMode(true, 0);
	  mixer.setVelocity(0.28);
	  sleep(4);
	  mixer.setVelocity(0);
	  state = 2;
	    cout << "Split is 1" << endl;
	  } else {
	   mixer.setVelocity(0);
	}
	break;
        case 1:
        {
          usleep(100000);
          pose.resetPose();
          cout<<"going slow and following line"<<endl;
          mixer.setVelocity(0.05);
          mixer.setEdgeMode(true, 0);
          usleep(1000000);
          state =2;
          break;
        }

        case 2:
        {
          //cout<<"sensor "<<dist.dist[0]<<endl;
          if(dist.dist[0]<0.2){
            //first pass
            cout<<"gate passing"<<endl;
            mixer.setVelocity(0.0);
            usleep(1000);
            state =3;
          }
          break;
        }

        case 3:
        {
          cout<<"check for opening"<<endl;
          //second pass to be sure it is a whole pass
          cout<<"sensor "<<dist.dist[0]<<endl;
          if(dist.dist[0]>0.35){
            cout<<"open"<<endl;
            //usleep(10000);
            mixer.setVelocity(0.45);
            usleep(4000000);
            state =4;
          }
          break;
        }

        case 4:
        {
          cout<<"end of axe gate"<<endl;
          mixer.setVelocity(0.0);
          finished = true;
          break;
        }

      }

    if (state != oldstate)
    {
      oldstate = state;
      toLog("state start");
      // reset time in new state
      t.now();
    }
    // wait a bit to offload CPU
    usleep(2000);
  
    if (lost)
    { // there may be better options, but for now - stop
      toLog("Plan40 got lost");
      mixer.setVelocity(0);
      mixer.setTurnrate(0);
    }
    //else
      //toLog("Plan40 finished");
  }
}


void BPlan40::terminate()
{ //
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void BPlan40::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}
