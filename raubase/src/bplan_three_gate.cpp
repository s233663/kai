/*
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "sdist.h"

#include "bplan_three_gate.h"

// create class object
BPlan_three_gate plan_three_gate;


void BPlan_three_gate::setup()
{ // ensure there is default values in ini-file
  if (not ini["plan_three_gate"].has("log"))
  { // no data yet, so generate some default values
    ini["plan_three_gate"]["log"] = "true";
    ini["plan_three_gate"]["run"] = "false";
    ini["plan_three_gate"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["plan_three_gate"]["print"] == "true";
  //
  if (ini["plan_three_gate"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_plan_three_gate.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plan_three_gate logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission status (mostly for debug)\n");
  }
  setupDone = true;
}

BPlan_three_gate::~BPlan_three_gate()
{
  terminate();
}


void BPlan_three_gate::run()
{
  if (not setupDone)
    setup();
  if (ini["plan_three_gate"]["run"] == "false")
    return;
  //
  UTime t("now");
  bool finished = false;
  bool lost = false;
  double length = 1;
  int angle = 0;
  state = 1;
  oldstate = state;
  float curr_h = 0;
  pose.resetPose();
  toLog("plan_three_gate started");
  //
  cout << "Three gates started" << endl;
  while (not finished and not lost and not service.stop)
  {
    switch (state)
      {

        case 1:
          cout<<"start"<<endl;
          mixer.setVelocity(0.3);
          mixer.setEdgeMode(true,0);
          if(medge.split>0)
            {
            pose.resetPose();
            cout<<"found split"<<endl;
            state = 101;
            }
          else
            usleep(10000);
        break;

        case 101:
	        mixer.setEdgeMode(true, 0);
          if (pose.dist > 0.6)
            {
              usleep(10000);
            }
          else
            state = 102;
        break;

        case 102:
          if (pose.dist > 1)
          {
            mixer.setVelocity(0.0);
            sleep(1);
	          pose.resetPose();
            mixer.setDesiredHeading(M_PI*0.3); //should rotate of 45 degrees on the left; correct with the correct value
            sleep(2);
            state=2;
          }
          else 
            usleep(10000);        
        break;

        case 2:
          cout<<"wait for regbot" << dist.dist[0] << endl;

	        if (dist.dist[0] < 0.4)
            { // something is close, assume it is the Regbot
              // start driving
              cout<<"found regbot"<<endl;
              sleep(2);
              mixer.setVelocity(0.1);
	            sleep(2);
              mixer.setEdgeMode(true,0);
              state = 201;
            }
          else 
            usleep(10000);
        break;

        case 201:
          if (medge.width > 0.05)
              {
                usleep(100000);
                pose.resetPose();
		            mixer.setEdgeMode(true, 0);
                state = 3;
                usleep(10000);
              }
          else 
            usleep(10000);
        break;

        case 3:
        	mixer.setEdgeMode(true, 0);
          cout<<"follow line until entering position"<<endl;
          if(pose.h > -M_PI/2)
          {
            if(dist.dist[0]<0.2)
            {
              cout<<"regbot is close"<<endl;
              mixer.setVelocity(0.0);
              sleep(2);
            }
            cout<<"continue"<<endl;
            mixer.setVelocity(0.25);
          }
          else{
            cout<<"curve finished"<<endl;
            pose.resetPose();
            state=301;
            usleep(10000);
          }
        break;

      case 301:
        if (pose.dist < 0.3)
        {
          if(dist.dist[0]<0.2)
          {
            cout<<"regbot is close"<<endl;
            mixer.setVelocity(0.0);
            sleep(2);
          }
          cout<<"continue"<<endl;
          mixer.setVelocity(0.25);
        }
        else
        {
          mixer.setVelocity(0.0);
          state = 4;
          usleep(10000);
        }
      break;

      case 4: //stop and turn left degrees & //go backward to climb the obstacle
          pose.resetPose();
          mixer.setDesiredHeading(M_PI*0.6);
          cout<<"turning to enter backwards"<<endl;
          sleep(2);
          //pose.turned=0;
          cout<<"start going backwards"<<endl;
          mixer.setVelocity(-0.25);
          state=7;
      break;

/*
      case 6: //when the rear wheels are on the top the distance sensor is pointing on the ground
      cout<<"climbing"<<endl;
      //cout<<"sensor sees "<<dist.dist[0]<<endl;
        if (dist.dist[0]< 0.4)
        {
            cout<<"wall almost climbed"<<endl;
            usleep(10000);
           // sleep(1); // this sleep is made in order to let the robot reach the center of the gates on the platform
            state = 7;
        }
        else
          usleep(10000);
      break;
*/

      case 7:
        if (dist.dist[0]>0.65) //the robot is now on the platform and ready to position itself for the circle
	      {
          cout<<"robot on the edge of the platform"<<endl;
          mixer.setVelocity(0.0);
          sleep(1);
          //put down the robotic arm and start going backward to be centered on the circle and calulcate the right distance from the center
          servo.setServo(4,1,0,0);
          sleep(1);
          mixer.setVelocity(-0.05);
          sleep(3);
          mixer.setVelocity(0);
          servo.setServo(4,1,-500,0); //position up to be calibrated
          sleep(2);
          pose.resetPose();
          mixer.setDesiredHeading(-M_PI*0.6); //clockwise
          cout<<"turn clockwise"<<endl;
          sleep(2);
          state = 8;
	      }
	      else
        {
          cout<<"going to climb the platform..."<<endl;
          usleep(10000);
        }

      break;

      case 8: //do the circle
        cout<<"start doing the circle"<<endl;
        curr_h = pose.h;
        mixer.setVelocity(0.2); //calculate the right value
        mixer.setTurnrate(-0.55); //calculate the right value
        usleep(200000);
        cout<<"Current heading = " << curr_h << endl;
        cout << "My pose is" << pose.h << endl;

        while (curr_h > pose.h)
        {
            usleep(10000);
            cout<<"I'm into the cycle, my pose is " << pose.h << endl;
        }

        usleep(100000);

        while(curr_h < pose.h)
        {
            usleep(10000);
        }

        mixer.setVelocity(0.0);
        mixer.setTurnrate(0.0);
        pose.resetPose();
        sleep(1);
        cout<<"finished circle and returned to initial position"<<endl;
        mixer.setDesiredHeading(M_PI*0.5);
        cout<<"turn in order to exit"<<endl;
        //circle complete
        sleep(1);
        state = 9;
      break;

      case 9: {
          //sleep(1);
          pose.resetPose();
          cout<<"pose reset"<<endl;
          
          if (dist.dist[0]<0.2)
          {
            cout<<"regbot passed"<<endl;
            usleep(1000000);
            mixer.setEdgeMode(false,0);
            mixer.setVelocity(0.1);
            usleep(1000000);
            medge.split=0;
            state = 10;
          }
          break;  
      }

      case 10: {
        cout<<"follow line until exiting position"<<endl;
        mixer.setEdgeMode(true,0);
        mixer.setVelocity(0.25);
        usleep(10000);

        if(dist.dist[0]<0.2)
        {
          cout<<"regbot is close"<<endl;
          mixer.setVelocity(0.0);
          sleep(2);
        }
        
        if (medge.split > 2)
        {
          cout<<"exited and going for the axe challenge"<<endl;
          sleep(3); //to be sure that it will be far from the regbot
          finished=true;
        }
      break;
      }

    }
    // switch (state)
    // { // make a shift in heading-mission
    //   case 10:    //     pose.resetPose();
    //     toLog("forward at 0.3m/s");
    //     mixer.setVelocity(0.3);
    //     state = 11;
    //     break;
    //   case 11: // sleep for distance
    //     if (pose.distist >= 1.0)
    //     { // done, and then
    //       finished = true;
    //     }
    //     else if (t.getTimePassed() > 10)
    //       lost = true;
    //     break;
    //   default:
    //     toLog("Unknown state");
    //     lost = true;
    //     break;
    // }
    if (state != oldstate)
    {
      oldstate = state;
      toLog("state start");
      // reset time in new state
      t.now();
    }
    // sleep a bit to offload CPU
    usleep(2000);
  }
  if (lost)
  { // there may be better options, but for now - stop
    toLog("plan_three_gate got lost");
    mixer.setVelocity(0);
    mixer.setTurnrate(0);
  }
  else
    toLog("plan_three_gate finished");
}


void BPlan_three_gate::terminate()
{ //
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void BPlan_three_gate::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}

