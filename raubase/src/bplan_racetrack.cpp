/*  
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "bplan20.h"
#include "cedge.h"
#include "sdist.h"

#include "bplan_racetrack.h"

// create class object
BPlan9_racetrack plan9;


void BPlan9_racetrack::setup()
{ // ensure there is default values in ini-file
  if (not ini["plan9"].has("log"))
  { // no data yet, so generate some default values
    ini["plan9"]["log"] = "true";
    ini["plan9"]["run"] = "false";
    ini["plan9"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["plan9"]["print"] == "true";
  //
  if (ini["plan9"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_plan9.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plan9 logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission status (mostly for debug)\n");
  }
  setupDone = true;
}

BPlan9_racetrack::~BPlan9_racetrack()
{
  terminate();
}


void BPlan9_racetrack::run()
{
  if (not setupDone)
    setup();
  if (ini["plan9"]["run"] == "false")
    return;
  //
  UTime t("now");
  bool finished = false;
  bool lost = false;
  state = 0; //race
  //state =100; //door closing
  oldstate = state;
  //float curr_h = 0.0;
  
  //
  toLog("plan9 started");
  //
  while (not finished and not lost and not service.stop)
  {
    switch (state)
    { // make a shift in heading-mission

      case 0: //100
      {
        //servo.setServo(4,1,500,1);
	sleep(2);
        pose.resetPose();
        usleep(5000);
        mixer.setVelocity(0.2);
        mixer.setEdgeMode(true,0);
        cout<<medge.width<<endl;
        state =1;
        break;
      }
      
      case 1:{
        if(dist.dist[1]<0.3){
          sleep(4);
          mixer.setVelocity(0.0);
          sleep(1);
          pose.resetPose();
	  mixer.setDesiredHeading(M_PI);
          sleep(3);
          pose.resetPose();
          mixer.setDesiredHeading(-0.1*M_PI);
          sleep(2);
          cout<<"end of line"<<endl;
          state =2; //101
        } else {
	  usleep(10000);
	}
        break;
      }
      
      case 2:{ //101
        //start race
        mixer.setEdgeMode(false,-0.05);
        sleep(1);
        mixer.setVelocity(0.4);
        cout<<"0.1"<<endl;
	mixer.setEdgeMode(false, 0);
        usleep(300000);
        mixer.setVelocity(0.9);
        cout<<"0.4"<<endl;
        usleep(500000);
        mixer.setVelocity(1.4);
        cout<<"0.8"<<endl;
        usleep(500000);
        mixer.setVelocity(1.9);
        cout<<"1"<<endl;
        usleep(500000);
        mixer.setVelocity(0.5);
	mixer.setEdgeMode(false, -0.07);
        cout<<"0.5"<<endl;
        usleep(3000000);
        //cout<<"line"<<medge.width<<endl;
        mixer.setVelocity(0.7);
        cout<<"speed 0.3"<<endl;
        usleep(2000000);
        mixer.setVelocity(0.5);
        cout<<"speed 0.5"<<endl;
        //usleep(5000000);
        //mixer.setVelocity(0.3);
        //cout<<"speed 0.3"<<endl;*/
        //usleep(1000000);
        state = 3;
        break;
      }

      case 3:{

        if(dist.dist[1]<0.4){
          mixer.setVelocity(0.0);
          sleep(1);
          pose.resetPose();
          mixer.setDesiredHeading(0);
        
          
          cout<<"end of race"<<endl;
          sleep(0.5);
          //finished =true;
          state =100;
          
        }
        break;
      }

      //closing doors

      case 100: //102
      {
        cout<<"going to close the doors"<<endl;
        
       // mixer.setEdgeMode(false,0);
        mixer.setVelocity(-0.1);
        usleep(4000000);
        pose.resetPose();
        //mixer.setVelocity(-0.1);
        usleep(1000000);
        cout<<"turn around"<<endl;
        mixer.setDesiredHeading(-M_PI/2);
        usleep(2000000);
        
        state = 101; //103
        break;
      }

      
      case 101: //103
      {
        mixer.setEdgeMode(false,0);
        usleep(1000000);
        //move until you are inside the box
        if(dist.dist[1]>0.35){
          cout<<"out of the box"<<endl;
          mixer.setVelocity(0.1);
          usleep(1000000);
        }
        else{
          cout<<"inside the box"<<endl;
          pose.resetPose();
          mixer.setVelocity(0);
          usleep(1000000);
          state =102; //104
        }
        
        break;
      }


      case 102: //104
      {
        cout<<"getting a bit out of the box"<<endl;
        mixer.setEdgeMode(true,0);
        mixer.setVelocity(-0.1);
        sleep(3);
        mixer.setVelocity(0);
        sleep(1);
        mixer.setDesiredHeading(M_PI/2);
        cout<<"turned left"<<endl;
        usleep(2000000);
        mixer.setVelocity(0.1);
        usleep(6000000);
        cout<<"moved a bit"<<endl;
        mixer.setVelocity(0);
        usleep(2000000);
        pose.resetPose();
        state =103; //105
        break;
      }

      case 103:{ //105
        cout<<"turned right"<<endl;
        //pose.resetPose();
        mixer.setDesiredHeading(-M_PI/2);
        sleep(2);
        mixer.setVelocity(0.1);
        pose.resetPose();
        usleep(1000000);
        state=104; //106
        break;
      }
      
      
      case 104:{ //106
        cout<<"searching for door"<<endl;
        if (pose.dist>1.1 and pose.dist<1.5){
          cout<<"found door"<<endl;
          //mixer.setVelocity(-0.1);
          //usleep(1500000);
          mixer.setVelocity(0);
          mixer.setDesiredHeading(-M_PI/2);
          usleep(2000000);
          pose.resetPose();
          mixer.setDesiredHeading(-M_PI*0.05);
          usleep(1000000);
          pose.resetPose();
          mixer.setVelocity(0.3);
          state =106; //108
        }
        else if(pose.dist>1.5){
          mixer.setVelocity(-0.1);
          cout<<"going backwards"<<endl;
          usleep(1000000);
          pose.resetPose();
          state = 105; //107
        }
        break;
      }

      case 105:{ //107
        if(dist.dist[1]<0.5){
          cout<<"found door"<<endl;
          usleep(4000000);
          mixer.setVelocity(0);
          cout<<"turned right"<<endl;
          mixer.setDesiredHeading(-M_PI/2);
          usleep(2000000);
          pose.resetPose();
          mixer.setVelocity(0.2);
          state =106; //108
        }
        break;
      }

      case 106:{ //108
        cout<<"distance for closing small door "<<pose.dist<<endl;
        //mixer.setVelocity(0.2);
        if(pose.dist>0.9){
          cout<<"reached position after box"<<endl;
          mixer.setVelocity(0);
          usleep(1000000);
          mixer.setDesiredHeading(-M_PI*0.55);
          usleep(1000000);
          pose.resetPose();
          mixer.setVelocity(0.2);
          state =107; //109
        }
        break;
      }

      case 107:{ //109
        cout<<"going to close big door"<<endl;
        cout<<"distance made = "<<pose.dist<<endl;
        if (pose.dist>1){
          cout<<"found door"<<endl;
          //mixer.setVelocity(0.1);
          //usleep(3000000);
          mixer.setVelocity(0);
          usleep(10000);
          pose.resetPose();
          mixer.setDesiredHeading(-M_PI*0.55);
          usleep(2000000);
          mixer.setVelocity(0.3);
          usleep(2000000);
          pose.resetPose();
          state =108; //111
        }
        else if(pose.dist>1.2){
          mixer.setVelocity(-0.1);
          cout<<"going backwards"<<endl;
          usleep(2000000);
          mixer.setVelocity(0);
          usleep(100);
          mixer.setDesiredHeading(-M_PI/2);
          usleep(2000000);
          cout<<"closing second door"<<endl;
          mixer.setVelocity(0.3);
          usleep(2000000);
          pose.resetPose();
          state = 108; //111
        }
        break;
      }

      case 108:{ //111
        cout<<"turning to finish"<<endl;
        mixer.setDesiredHeading(M_PI/2);
        mixer.setVelocity(0);
        usleep(100000);
        mixer.setVelocity(-0.1);
        usleep(3000000);
        mixer.setVelocity(0);
        sleep(1);
        mixer.setEdgeMode(true,0);
        mixer.setVelocity(0.2);
        usleep(100000);
        state =109;
        break;
      }

      case 109: { //112
        if(dist.dist[1]<0.4){
          sleep(2);
          mixer.setVelocity(0);
          cout<<"finished"<<endl;
          finished =true;
        }
        break;
      }

      /*
      case 0:
        printf("I'm in state 0\n");
        pose.resetPose();
        mixer.setVelocity(0.2);
        mixer.setEdgeMode(true,0);
        usleep(10000);
        state=1;
        break;
      case 1:
	      //mixer.setEdgeMode(true, 0);
        //mixer.setVelocity(0.7); 
        //pose.resetPose();
	      printf("I'm in state 1\n");
        if(pose.dist >2.6)
        {
          printf("i'm more than 1 meter");
          mixer.setVelocity(0);
          usleep(1000000);
          pose.resetPose();
          mixer.setDesiredHeading(-M_PI);
          
          usleep(2000000);
          mixer.setDesiredHeading(-M_PI*0.8);
          // state = 10;
          usleep(2000000);
          mixer.setEdgeMode(true,0);
          cedge.changePID(23,0.5,0.02,0.0);
          usleep(2000000);
          mixer.setVelocity(0.7);
          
          state = 21;
          
        
        }
        else
        {
          usleep(100000);
        }
        break;
      

        */
      
      
      case 21:
      //start race
        //pose.resetPose();
        // mixer.setVelocity(0.3);
        // usleep(1000000);
        // mixer.setVelocity(0.8);
        // usleep(1000000);
        // mixer.setVelocity(1.3);
        // usleep(1000000);
        // mixer.setVelocity(1.5);
       
        // cedge.changePID(23,0.5,0.02,0.0);
        // usleep(100000);
      
        //cout<<pose.dist<<endl;
        // state=22;
         mixer.setEdgeMode(true,0);
          // cedge.changePID(23,0.5,0.02,0.0);
          cedge.changePID(20,0.6,0.15,0.0);
          usleep(2000000);
          mixer.setVelocity(0.6);
         state = 22;
          break;


      case 22:
        if(pose.dist > 2)
          {
            // cedge.changePID(20,0.6,0.15,0.0);
            mixer.setEdgeMode(true, 0.02);
            // mixer.setVelocity(0.3);
            
            usleep(100000);
            //state =23;
            
          }
          
        else{
          usleep(100000);
        }
        toLog("reached turn");
      break;

      case 23:
        pose.turned = 0.0;
          //mixer.setVelocity(0.8);
          // cedge.changePID(23,0.5,0.02,0.0);
        mixer.setEdgeMode(true, 0.03); 
        state = 31;
	break;


      case 31: // wait for distance
        while(pose.turned < -M_PI/2)
        { 
          // cedge.changePID(23,0.5,0.02,0.0);
          usleep(10000);


        }
        mixer.setEdgeMode(false, 0); 
        mixer.setVelocity(2);
        sleep(100000);
        pose.dist = 0.0;
        state = 32;
        break;
      case 32: 
        while (pose.dist < 1.85 )
        {
          
          usleep(10000);
          
        }
         toLog("reached turn again");
         pose.turned = 0.0;
        mixer.setVelocity(0.9);
        // cedge.changePID(23,0.5,0.02,0.0);
        mixer.setEdgeMode(false, 0.03); 
          state = 33;
        
        break;
        case 33:
        //reach second turn
        while (pose.turned < -M_PI/2)
        {    
        usleep(100000);

        }
        mixer.setEdgeMode(false, 0); 
        mixer.setVelocity(2);
        pose.dist = 0.0;
        state = 34;
	break;
      case 35:
        pose.resetPose();
        while(pose.dist<0.82)
        {
          usleep(100000);
        }
        mixer.setVelocity(0);
	break;
      default:
        toLog("Unknown state");
        lost = true;
        break;
    
    }

    if (state != oldstate)
    {
      oldstate = state;
      toLog("state start");
      // reset time in new state
      t.now();
    }
    // wait a bit to offload CPU
    usleep(2000);
  }
  if (lost)
  { // there may be better options, but for now - stop
    toLog("plan9 got lost");
    mixer.setVelocity(0);
    mixer.setTurnrate(0);
  }
  else
    toLog("plan9 finished");
}


void BPlan9_racetrack::terminate()
{ //
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void BPlan9_racetrack::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}

