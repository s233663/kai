/*  
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "sdist.h"

#include "bplandrive12to10.h"
#include "simu.h"

// create class object
BPlandrive12to10 plandrive12to1020;


void BPlandrive12to10::setup()
{ // ensure there is default values in ini-file
  if (not ini["plandrive12to10"].has("log"))
  { // no data yet, so generate some default values
    ini["plandrive12to10"]["log"] = "true";
    ini["plandrive12to10"]["run"] = "false";
    ini["plandrive12to10"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["plandrive12to10"]["print"] == "true";
  //
  if (ini["plandrive12to10"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_plandrive12to10.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plandrive12to10 logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission status (mostly for debug)\n");
  }
  setupDone = true;
}

BPlandrive12to10::~BPlandrive12to10()
{
  terminate();
}


void BPlandrive12to10::run()
{

  


  if (not setupDone)
    setup();
  if (ini["plandrive12to10"]["run"] == "false")
    return;
  //
  UTime t("now");
  bool finished = false;
  bool lost = false;
  double length = 1;
  state = 6;
  oldstate = state;
  int distt =0 ;


  toLog("plandrive12to10 started");
  //

  int state = 0;
  pose.resetPose();
  while (not finished and not lost and not service.stop)
  {





///drive chalenge 12 to challenge 10


    switch (state) {

      case 0:
       //cout << "case 0 width"<< medge.width << endl;
        mixer.setEdgeMode(true, 0);
	      mixer.setVelocity(0.4);
        if(medge.width> 0.055){
          sleep(1);
        state= 1;
        }
      break;

      case 1:
    //cout << "case 1" << endl;
        mixer.setEdgeMode(true, 0);
	      mixer.setVelocity(0.4);
      // cout << "width " << medge.width << endl;
        if(medge.width> 0.08){
          state= 2; 
        } 
       break;
    
      case 2:
//cout << "case 2" << endl;
        mixer.setVelocity(0);
        mixer.setDesiredHeading(3.14*0.75);
        sleep(1);
   
        mixer.setVelocity(0.2);
        mixer.setEdgeMode(false, 0);
   //cout << "way to axe" << endl;
        usleep(5000);

         state = 3;
      break;

      case 3:
      //cout << "case 3" << endl;
      //out << "split " << medge.width << endl;
        if(medge.width> 0.08){  
  //cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
        mixer.setVelocity(0);
        pose.resetPose();
        mixer.setDesiredHeading(-3.14*0.75);
        sleep(2);
  
        state = 4;
        } 
      break;

      case 4:
        mixer.setVelocity(0.3);
        mixer.setEdgeMode(false, 0);
        finished = true;
  // cout << "facing axe" << endl;
      break;
    }
  usleep(500);


// end drivech12to10






   
    if (state != oldstate)
    {
      oldstate = state;
      toLog("state start");
      // reset time in new state
      t.now();
    }
    // wait a bit to offload CPU
    usleep(2000);
  }


  if (lost)
  { // there may be better options, but for now - stop
    toLog("plandrive12to10 got lost");
    mixer.setVelocity(0);
    mixer.setTurnrate(0);
  }
  else
    toLog("Plandrive12to10 finished");
}


void BPlandrive12to10::terminate()
{ //
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void BPlandrive12to10::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}

