/*  
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "sdist.h"

#include "bplan20.h"

// create class object
BPlan20 plan20;


void BPlan20::setup()
{ // ensure there is default values in ini-file
  if (not ini["plan20"].has("log"))
  { // no data yet, so generate some default values
    ini["plan20"]["log"] = "true";
    ini["plan20"]["run"] = "false";
    ini["plan20"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["plan20"]["print"] == "true";
  //
  if (ini["plan20"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_plan20.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plan20 logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission status (mostly for debug)\n");
  }
  setupDone = true;
}

BPlan20::~BPlan20()
{
  terminate();
}


void BPlan20::run()
{
  if (not setupDone)
    setup();
  if (ini["plan20"]["run"] == "false")
    return;
  //
  UTime t("now");
  bool finished = false;
  bool lost = false;
  double length = 1;
  state = 1;
  oldstate = state;
  int distt =0 ;
  //cedge.changePID(23,0.5,0.02,0.0);
  toLog("Plan20 started");
  //
          pose.resetPose();
	        
  while (not finished and not lost and not service.stop)
  {
    switch (state)
      {

        case 1:
          mixer.setVelocity(0.2);
          mixer.setEdgeMode(true, 0);
          

          if (medge.width > 0.05){
            usleep(3700000);
            mixer.setVelocity(0.0);
            state = 2;
          }
          break;
         
        case 2:
        
          if (dist.dist[1] < 0.40){
            usleep(1000000);
            mixer.setVelocity(0.3);

            state = 3;
          }
  



        break;




        case 3:
        mixer.setVelocity(0.3);
          mixer.setEdgeMode(true, 0);
            if (medge.width > 0.05){
               usleep(1000000);
               mixer.setVelocity(0.07);
            state = 6;
          }

        break;



      case 6:
	mixer.setEdgeMode(true, 0);
	mixer.setVelocity(0.07);
	while (medge.split == 0) {
	  usleep(100000);
	}
	state = 7;
	break;
      case 7:
	pose.resetPose();
	mixer.setEdgeMode(true, 0);
	mixer.setVelocity(0.2);
	while(pose.dist < 1) {
	  usleep(100000);
	}
	mixer.setVelocity(0);
	state = 8;
	break;

      case 8:
	while(dist.dist[0] > 0.4) {
	  usleep(100000);
	}
	usleep(200000);
	state = 12;
	break;
      case 10:
	cout << "Insert the length\n";
	// scanf("%lf", &length);
	cout << "Advancing " << length << endl;
  cout<<"decide distance\n"<<endl;
 // cin>>distt;
  cout<<"finish"<<endl;
	pose.resetPose();
	mixer.setVelocity(0.3);
	mixer.setEdgeMode(false, 0);
  cout<<"Check"<<endl;
  while(true) {
    cout<<dist.dist[0]<<endl;
    usleep(500);
    if(dist.dist[0]<0.5)
  {
    mixer.setVelocity(0);
    state = 12;
    break;
  }

  }
  
  // if(dist.dist[0] <= distt)
  // {
  //   mixer.setVelocity(0);
  // }
	break;
      case 11:
      while(true) 
      {
       if(dist.dist[0] <= 0.5)
    {
      cout<<"obstacle"<<endl;
      mixer.setVelocity(0);
    state = 12;
    break;
    }
    
      }
  
	break;
      case 12: 
	cout<<dist.dist[0]<<endl;
	if(dist.dist[0] >= 0.4)
	  {
	    usleep(100000);
	    cout<<"no obstacle"<<endl;
	    mixer.setVelocity(0.65);
	    state = 13;
      sleep(2);
	    cout << "passed to state 13" << endl;
	  }

	if(dist.dist[0] < 0.4)
	  {
	    cout<<"obstacle"<<endl;
	    mixer.setVelocity(0);
	  }
  
    
	usleep(2);
      break;
    // if((t.getTimePassed() > 8))
    // {
    //   mixer.setVelocity(0);
    // }
		// cout << "Insert the angle\n";
		// scanf("%d", &angle);
		// mixer.setVelocity(0);
		// mixer.setDesiredHeading(angle * M_PI/180);
		// state = 10;
	break;
      }
    // switch (state)
    // { // make a shift in heading-mission
    //   case 10:    //     pose.resetPose();
    //     toLog("forward at 0.3m/s");
    //     mixer.setVelocity(0.3);
    //     state = 11;
    //     break;
    //   case 11: // wait for distance
    //     if (pose.dist >= 1.0)
    //     { // done, and then
    //       finished = true;
    //     }
    //     else if (t.getTimePassed() > 10)
    //       lost = true;
    //     break;
    //   default:
    //     toLog("Unknown state");
    //     lost = true;
    //     break;
    // }
    if (state != oldstate)
    {
      oldstate = state;
      toLog("state start");
      // reset time in new state
      t.now();
    }
    // wait a bit to offload CPU
    usleep(2000);
  }
  if (lost)
  { // there may be better options, but for now - stop
    toLog("Plan20 got lost");
    mixer.setVelocity(0);
    mixer.setTurnrate(0);
  }
  else
    toLog("Plan20 finished");
}


void BPlan20::terminate()
{ //
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void BPlan20::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}

