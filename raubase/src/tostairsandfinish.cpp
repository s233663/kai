/*  
 * 
 * Copyright © 2023 DTU,
 * Author:
 * Christian Andersen jcan@dtu.dk
 * 
 * The MIT License (MIT)  https://mit-license.org/
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software 
 * is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies 
 * or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
 * FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, 
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN 
 * THE SOFTWARE. */

#include <string>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "mpose.h"
#include "steensy.h"
#include "uservice.h"
#include "sencoder.h"
#include "utime.h"
#include "cmotor.h"
#include "cservo.h"
#include "medge.h"
#include "cedge.h"
#include "cmixer.h"
#include "simu.h"


#include "sdist.h"
#include "tostairsandfinish.h"


// create class object
BPlan100 plan100;


void BPlan100::setup()
{ // ensure there is default values in ini-file
  if (not ini["plan100"].has("log"))
  { // no data yet, so generate some default values
    ini["plan100"]["log"] = "true";
    ini["plan100"]["run"] = "false";
    ini["plan100"]["print"] = "true";
  }
  // get values from ini-file
  toConsole = ini["plan100"]["print"] == "true";
  //
  if (ini["plan100"]["log"] == "true")
  { // open logfile
    std::string fn = service.logPath + "log_plan100.txt";
    logfile = fopen(fn.c_str(), "w");
    fprintf(logfile, "%% Mission plan100 logfile\n");
    fprintf(logfile, "%% 1 \tTime (sec)\n");
    fprintf(logfile, "%% 2 \tMission state\n");
    fprintf(logfile, "%% 3 \t%% Mission status (mostly for debug)\n");
  }
  setupDone = true;
}

BPlan100::~BPlan100()
{
  terminate();
}


void BPlan100::run()
{
  if (not setupDone)
    setup();
  if (ini["plan100"]["run"] == "false")
    return;
  //
  UTime t("now");
  bool finished = false;
  bool lost = false;
  state = 10;
  oldstate = state;
  //
  toLog("Plan100 started");
  //
  while (not finished and not lost and not service.stop)
  {


int caaase = 1;
bool notdone = true;
while(notdone){   ///programm from roundabout to ramp to the stairs
switch(caaase){

  case 1:
  mixer.setEdgeMode(true, 0);
    mixer.setVelocity(0.5);
    cout << "width 1   " << medge.width << endl;
      if(medge.width > 0.04){
        caaase = 2;
        sleep(1);
      }
  break;

  case 2:
  cout << "width 2  " << medge.width << endl;
   if(medge.width > 0.06){
         mixer.setVelocity(0.2);
         pose.resetPose();
        sleep(0.8);
        mixer.setVelocity(0);
         mixer.setDesiredHeading(2.50);
        sleep(1);
        caaase = 3;
      }

  break;

  case 3:
  mixer.setEdgeMode(false, 0);
 mixer.setVelocity(0.4);
  if(medge.width > 0.05){
    sleep(1);
      notdone = false;

  }
  break;
}



usleep(200);
}     //end while

 /////////end drive to stairs







//start code down the stairs on the ramp
cout << "start stairs" << endl;
int caase = 1;
pose.resetPose();
while(1){

switch (caase)
{
case 1:

      mixer.setVelocity(0.2);
      mixer.setEdgeMode(true, 0);
     if(medge.width> 0.05){
        usleep(3000000);
      caase = 2;
      }
  break;

case 2:
 mixer.setVelocity(0);
pose.resetPose();
cout << "stop " << endl;
sleep(0.5);
 mixer.setDesiredHeading((3.1415));
 cout << "turn" << endl;
 sleep(3);
 pose.resetPose();
 mixer.setDesiredHeading((3.1415 * 0.2));
 sleep(1);
 mixer.setVelocity(-0.2);
 cout << "back" << endl;
 sleep(0.5);
 // mixer.setVelocity(0.1);
 cout << "aling" << endl;
 //sleep(0.5);
 caase = 3;
break;

case 3:
mixer.setVelocity(-0.4);
//cout << "gyro "<< imu.gyro[1]  << endl;
//cout << "gyro "<< imu.gyro[1]  << endl;
  if(imu.gyro[1]  > 150){
    cout << "1st drop" << endl;
    cout << "gyro "<< imu.gyro[1]  << endl;
    usleep(350000);
    mixer.setVelocity(0.0);
       usleep(800000);
    mixer.setVelocity(0.2);
usleep(900000);
    caase = 4;
  }

break;

case 4:
mixer.setVelocity(-0.4);
//cout << "gyro "<< imu.gyro[1]  << endl;
//cout << "gyro "<< imu.gyro[1]  << endl;
  if(imu.gyro[1]  > 150){
    cout << "2nd drop" << endl;
    cout << "gyro "<< imu.gyro[1]  << endl;
    usleep(350000);
    mixer.setVelocity(0.0);
       usleep(800000);
    mixer.setVelocity(0.2);
usleep(900000);
    caase = 5;
  }

break;
case 5:
mixer.setVelocity(-0.4);
//cout << "gyro "<< imu.gyro[1]  << endl;
//cout << "gyro "<< imu.gyro[1]  << endl;
  if(imu.gyro[1]  > 150){
    cout << "3rd drop" << endl;
    cout << "gyro "<< imu.gyro[1]  << endl;
    usleep(350000);
    mixer.setVelocity(0.0);
       usleep(800000);
    mixer.setVelocity(0.2);
usleep(900000);
    caase = 6;
  }

break;


case 6:
mixer.setVelocity(-0.4);
//cout << "gyro "<< imu.gyro[1]  << endl;
//cout << "gyro "<< imu.gyro[1]  << endl;
  if(imu.gyro[1]  > 150){
    cout << "4th drop" << endl;
    cout << "gyro "<< imu.gyro[1]  << endl;
    usleep(350000);
    mixer.setVelocity(0.0);
       usleep(800000);
    mixer.setVelocity(0.2);
usleep(900000);
    caase = 7;
  }

break;


case 7:
mixer.setVelocity(-0.4);
//cout << "gyro "<< imu.gyro[1]  << endl;
//cout << "gyro "<< imu.gyro[1]  << endl;
  if(imu.gyro[1]  > 150){
    cout << "5th drop" << endl;
    cout << "gyro "<< imu.gyro[1]  << endl;
    usleep(350000);
    mixer.setVelocity(0.0);
       usleep(800000);
    mixer.setVelocity(0.2);
usleep(900000);
    caase = 8;
    mixer.setVelocity(-0.2);
sleep(1);
mixer.setVelocity(0);
sleep(1);
pose.resetPose();
mixer.setDesiredHeading(-3.6);
sleep(2);
mixer.setVelocity(0.2);
mixer.setEdgeMode(false, 0);
  cout << "look for line" << endl;
  }

break;

case 8:

if (medge.width > 0.05){
  cout << "intersection found" << endl;
  mixer.setVelocity(0);
  mixer.setDesiredHeading(1*3.1415);
  sleep(5);
    cout << "heading found" << endl;
  mixer.setEdgeMode(true, 0);
  mixer.setVelocity(0.1);
  caase = 9;
}

  
break;

case 9:
  cout << "dis  " << dist.dist[0] << endl;
mixer.setVelocity(0.2);
  if(dist.dist[0] < 0.10){
    sleep(3);
       cout << "end" << endl;
       caase = 10;
  }
break;

case 10:
mixer.setVelocity(0);
break;
}


usleep(200);
}  //end of code down the stairs and to the finish


///////end programm











/*

//start code - up the ramp and down the seesaw

 cout << "diving up the ramp" << endl;
int status = 1;
pose.resetPose();
while (not finished){
  switch (status)
  {
    case 1: 
    
      mixer.setVelocity(0.5);
      mixer.setEdgeMode(false, 0);
      if (medge.width> 0.05){       // detect first intersection
        status = 2;
        cout << "detect first intersection" << endl;
        mixer.setVelocity(0.15);
        usleep(900000);
       // pose.resetPose();
      }
    break;

    case 2:
     //cout << "detect first intersection " << endl;
      if (medge.width> 0.05){       // detect second intersection
        mixer.setVelocity(0);
        pose.resetPose();
        cout << "Pose after reset" << pose.turned /3.14 << endl;
        sleep(0.5);
        mixer.setVelocity(-0.1);
         cout << "Pose " << pose.turned /3.14 << endl;
         sleep(1);
         mixer.setVelocity(0.1);
        mixer.setDesiredHeading((3.14 * -0.8));
        sleep(1);
        //pose.resetPose();
       // mixer.setDesiredHeading((3.14 * -0.8));
        //sleep(3);
         cout << "Pose 2 " << pose.turned /3.14 << endl;
         cout << "detect secound intersection" << endl;
        status = 3;
        //sleep(5);

        
      }
    break;
  
    case 3:
 mixer.setVelocity(0.08);
 mixer.setEdgeMode(true, 0);
     cout << "Pose 3 " << pose.turned /3.14 << endl;
     sleep(2);
     status = 4;
    break;




    case 4:         //now on seesaw
       cout << "case 4" << endl;
      if(medge.width> 0.05){  //detect new line
   
        sleep(3);
        mixer.setVelocity(0.2);
         cout << "after seesaw" << endl;
        finished = true;
      }
    break;

  
  }



usleep(200);
}       // end while


*/





/*

    //original code
    switch (state)
    { // make a shift in heading-mission
      case 10:
        toLog("Reset pose");
        pose.resetPose();

        toLog("forward at 0.3m/s");
        mixer.setVelocity(0.3);
        state = 11;
        break;
      case 11: // wait for distance
        if (pose.dist >= 0.3)
        { // done, and then
          toLog("now turn at 0.5 rad/s and 0 m/s");
          // reset turned angle
          pose.turned = 0.0;
          mixer.setVelocity(0.0);
          mixer.setTurnrate(0.5);
          state = 21;
        }
        else if (t.getTimePassed() > 10)
          lost = true;
        break;
      case 21:
        if (pose.turned >= M_PI)
        {
          mixer.setDesiredHeading(M_PI);
          toLog("now go back");
          mixer.setVelocity(0.3);
          // reset driven distance
          pose.dist = 0;
          state = 31;
        }
        else if (t.getTimePassed() > 12)
          lost = true;
        break;
      case 31: // wait for distance
        if (pose.dist >= 0.3)
        { // the end
          mixer.setVelocity(0.0);
          finished = true;
        }
        else if (t.getTimePassed() > 10)
          lost = true;
        break;
      default:
        toLog("Unknown state");
        lost = true;
        break;
    }   // end original code
*/


    if (state != oldstate)
    {
      oldstate = state;
      toLog("state start");
      // reset time in new state
      t.now();
    }
    // wait a bit to offload CPU
    usleep(2000);
  }

  
  if (lost)
  { // there may be better options, but for now - stop
    toLog("Plan100 got lost");
    mixer.setVelocity(0);
    mixer.setTurnrate(0);
  }
  else
    toLog("Plan100 finished");
}


void BPlan100::terminate()
{ //
  if (logfile != nullptr)
    fclose(logfile);
  logfile = nullptr;
}

void BPlan100::toLog(const char* message)
{
  UTime t("now");
  if (logfile != nullptr)
  {
    fprintf(logfile, "%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
            oldstate,
            message);
  }
  if (toConsole)
  {
    printf("%lu.%04ld %d %% %s\n", t.getSec(), t.getMicrosec()/100,
           oldstate,
           message);
  }
}

